const mongoClient = require('mongodb').MongoClient;
const Constants = require('../../constants/constants');
class UserMapper {
    static async createUser(user) {
        console.log(user);
        return new Promise(async (resolve, reject) => {
            try {
               let client = await mongoClient.connect(
                    Constants.DB_CONNECTION_STRING,
                    {
                        useUnifiedTopology: true
                    });
                console.log('got connection',Constants.DB_DATABASE_NAME);
                // database to be used
                let userDatabase = client.db(Constants.DB_DATABASE_NAME);
                console.log('created userDatabase')
                // collection to be used
                let collection = userDatabase.collection(Constants.DB_COLLECTION_NAME);
                console.log('created collection')
                collection.insertOne(user, function(err, result){
                    if (err) {
                        console.log(err);
                        client.close();
                        throw err;
                    }
                    console.log('user mapper',result);
                    client.close();
                    resolve(result);
                });
            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }
    static async findUser(employeeID) {
        console.log("findUser entry", employeeID);
        return new Promise(async (resolve, reject) => {
            try {
                let client = await mongoClient.connect(
                    Constants.DB_CONNECTION_STRING,
                    {
                        useUnifiedTopology: true
                    });
                console.log('got connection',Constants.DB_COLLECTION_NAME);
                // database to be used
                let userDatabase = client.db(Constants.DB_DATABASE_NAME);
                console.log('created userDatabase')
                // collection to be used
                let collection = userDatabase.collection(Constants.DB_COLLECTION_NAME);
                console.log('created collection')
                collection.findOne({"employeeID":employeeID}, function(err, result){
                    if (err) {
                        console.log(err);
                        client.close();
                        throw err;
                    }
                    console.log('user mapper',result);
                    client.close();
                    resolve(result);
                });
            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }
    static async deleteUser(employeeID) {
        console.log("delete User entry", employeeID);
        return new Promise(async (resolve, reject) => {
            try {
                let client = await mongoClient.connect(
                    Constants.DB_CONNECTION_STRING,
                    {
                        useUnifiedTopology: true
                    });
                console.log('got connection',Constants.DB_COLLECTION_NAME);
                // database to be used
                let userDatabase = client.db(Constants.DB_DATABASE_NAME);
                console.log('created userDatabase')
                // collection to be used
                let collection = userDatabase.collection(Constants.DB_COLLECTION_NAME);
                console.log('created collection')
                collection.deleteOne({"employeeID":employeeID}, function(err, result){
                    if (err) {
                        console.log(err);
                        client.close();
                        throw err;
                    }
                    console.log('user mapper',result);
                    client.close();
                    resolve(result);
                });
            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }
    static async updateUser(employeeID,updateValue) {
        console.log("update User entry", employeeID,updateValue);
        return new Promise(async (resolve, reject) => {
            try {
                let client = await mongoClient.connect(
                    Constants.DB_CONNECTION_STRING,
                    {
                        useUnifiedTopology: true
                    });
                console.log('got connection',Constants.DB_COLLECTION_NAME);
                // database to be used
                let userDatabase = client.db(Constants.DB_DATABASE_NAME);
                console.log('created userDatabase')
                // collection to be used
                let collection = userDatabase.collection(Constants.DB_COLLECTION_NAME);
                console.log('created collection')
                collection.updateOne({"employeeID":employeeID},{$set:updateValue},{upsert:true}, function(err, result){
                    if (err) {
                        console.log(err);
                        client.close();
                        throw err;
                    }
                    console.log('user mapper',result);
                    client.close();
                    resolve(result);
                });
            } catch (e) {
                console.log(e)
                reject(e);
            }
        });
    }
}
export const createUserMapper = UserMapper.createUser;
export const findUserMapper = UserMapper.findUser;
export const deleteUserMapper = UserMapper.deleteUser;
export const updateUserMapper = UserMapper.updateUser;