import {createUserMapper,findUserMapper,deleteUserMapper,updateUserMapper} from "../../mapper/user/usermapper";
const Constants = require('../../constants/constants');
class UserDelegate {
    static async createUser(user)  {
        console.log('user delegate',user);
        return new Promise(async (resolve, reject) => {
            try {
                //TODO check whether user is already present
                let result = await createUserMapper(user);
                console.log('user delegate', result);
                const response = {
                    "statusCode": Constants.SUCCESS_STATUS_CODE,
                    "body": {
                        message: Constants.SUCCESS_USER_CREATED
                    },
                }
                resolve(response);
            } catch (e) {
                console.log('user delegate', e);
                let errorMessage = Constants.ERROR_SOMETHING_BAD
                reject(errorMessage);
                reject(e);
            }
        })
    }
    static async findUser(employeeID)  {
        console.log('user delegate',employeeID);
        return new Promise(async (resolve, reject) => {
            try {
                let result = await findUserMapper(employeeID);
                console.log('user delegate', result);
                if(result !== null) {
                    const response = {
                        "statusCode": Constants.SUCCESS_STATUS_CODE,
                        "user": result
                    }
                    resolve(response);
                } else {
                    let errorMessage = Constants.ERROR_USER_NOT_FOUND
                   reject(errorMessage);
                }
            } catch (e) {
                console.log('user delegate', e);
                let errorMessage = Constants.ERROR_SOMETHING_BAD
                reject(errorMessage);
            }
        })
    }
    static async deleteUser(employeeID)  {
        console.log('user delegate',employeeID);
        return new Promise(async (resolve, reject) => {
            try {
                let result = await deleteUserMapper(employeeID);
                console.log('user delegate', result);
                let value = result["value"];
                if(value !== null) {
                    const response = {
                        "statusCode": Constants.SUCCESS_STATUS_CODE,
                        "message": Constants.SUCCESS_USER_DELETED
                    }
                    resolve(response);
                } else {
                    let errorMessage = Constants.ERROR_USER_NOT_FOUND
                    reject(errorMessage);
                }
            } catch (e) {
                console.log('user delegate', e);
                let errorMessage = Constants.ERROR_SOMETHING_BAD
                reject(errorMessage);
            }
        })
    }
    static async updateUser(employeeID, updateValue)  {
        console.log('user delegate',employeeID);
        return new Promise(async (resolve, reject) => {
            try {
                let result = await updateUserMapper(employeeID,updateValue);
                console.log('user delegate', result);
                if(result !== null) {
                    const response = {
                        "statusCode": Constants.SUCCESS_STATUS_CODE,
                        "message": Constants.SUCCESS_USER_UPDATED
                    }
                    resolve(response);
                } else {
                    let errorMessage = Constants.ERROR_USER_NOT_FOUND
                    reject(errorMessage);
                }
            } catch (e) {
                console.log('user delegate', e);
                let errorMessage = Constants.ERROR_SOMETHING_BAD
                reject(errorMessage);
            }
        })
    }
}
export const createUserDelegate = UserDelegate.createUser;
export const findUserDelegate = UserDelegate.findUser;
export const deleteUserDelegate = UserDelegate.deleteUser;
export const updateUserDelegate = UserDelegate.updateUser;