module.exports = {
    DB_COLLECTION_NAME: process.env.DB_COLLECTION_NAME,
    DB_DATABASE_NAME: process.env.DATABASE_NAME,
    DB_CONNECTION_STRING: process.env.DB_CONNECTION_STRING,
    ERROR_USER_NOT_FOUND: "[400] User not found",
    ERROR_SOMETHING_BAD: "[500] Something wrong happened",
    SUCCESS_USER_UPDATED: "User updated successfully",
    SUCCESS_USER_CREATED: "User created successfully",
    SUCCESS_USER_DELETED: "User deleted successfully",
    SUCCESS_STATUS_CODE: 200
}