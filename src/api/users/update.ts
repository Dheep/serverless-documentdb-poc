import {updateUserDelegate} from "../../delegate/user/userdelegate";

class Update {
    static async updateUser(event, context, callback) {
        try {
            console.log(event, context);
            const employeeID = event.path.employeeID;
            const updateValue = event.body;
            console.log('api',employeeID);
            let result = await updateUserDelegate(employeeID,updateValue)
            console.log('api result',result);
            callback(null,result);
        } catch (e) {
            console.log(e);
            callback(new Error(e));
        }
    }

}


export const updateUser = Update.updateUser;

