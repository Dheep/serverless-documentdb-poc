import {createUserDelegate} from "../../delegate/user/userdelegate";

class Create {
    static async createUser(event, context, callback) {
            try {
                console.log(event, context);
                let user = event.body;
                console.log('api',user);
                let result = await createUserDelegate(user)
                console.log('api create user',result)
                callback(null,result);
            } catch (e) {
                console.log(e);
                callback(new Error(e));
            }
    }

}


export const createUser = Create.createUser;

