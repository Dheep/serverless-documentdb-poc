import {deleteUserDelegate} from "../../delegate/user/userdelegate";

class Find {
    static async deleteUser(event, context, callback) {
        try {
            console.log(event, context);
            const employeeID = event.path.employeeID;
            console.log('api',employeeID);
            let result = await deleteUserDelegate(employeeID)
            console.log('api result',result);
            callback(null,result);
        } catch (e) {
            console.log(e);
            callback(new Error(e));
        }
    }

}


export const deleteUser = Find.deleteUser;

